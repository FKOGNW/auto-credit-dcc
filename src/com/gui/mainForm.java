package com.gui;

import com.opencsv.CSVReader;
import com.opencsv.CSVReaderBuilder;
import com.opencsv.CSVWriter;
import com.opencsv.exceptions.CsvException;
import com.toedter.calendar.JDateChooser;
import com.toedter.calendar.JTextFieldDateEditor;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.io.*;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

public class mainForm {
    private JPanel panel1;
    private JButton btConvert;
    private JLabel label1;
    private JComboBox<String> cmbUploadType;
    private JTextField tfDebitAccount;
    private JTextField tfAccountDescription;
    private JComboBox cmbInstructionAt;
    private JDateChooser fieldIntrustionDate;
    private JComboBox cmbChargeTo;
    private JComboBox cmbChargeType;
    private static JFrame parent;

    private static Properties prop;

    private static SimpleDateFormat format;

    private static String debitAccount;
    private static String descriptionAccount;
    private static String instructionAt="";
    private static String instructionDate="";
    private static String chargeType;
    private static String chargeTo;

    private static String productCode;

    private static Long startTime;

    public static void main(String[] args){
        prop= new Properties();
        String properties= "app.config";

        InputStream config = null;
        try {
            config = new FileInputStream(properties);
            prop.load(config);
        } catch (IOException e) {
            e.printStackTrace();
        }
        showGUI();
    }

    private void createUIComponents() {
        fieldIntrustionDate = new JDateChooser();
    }

    private mainForm(){
        parent = new JFrame();
        /**
         * HAC= Inhouse
         * HYR= Payroll
         * */
        cmbUploadType.addItem("Inhouse");
        cmbUploadType.addItem("Payroll");
        String uploadType=prop.getProperty("app.bulk_upload");
        if(checkingUploadTypeConfig(uploadType)!=null){
            cmbUploadType.setSelectedItem(checkingUploadTypeConfig(uploadType));
        }else{
            JOptionPane.showMessageDialog(parent, "Upload type not match.\n Please set correct upload type in config file.");
            System.exit(1);
        }

        /**
         * OUR= REM
         * BEN= BEN
         * */
        cmbChargeTo.addItem("REM");
        cmbChargeTo.addItem("BEN");
        String chargeToTemp=prop.getProperty("app.charge_to");
        if (checkingChargeToConfig(chargeToTemp)!=null){
            cmbChargeTo.setSelectedItem(checkingChargeToConfig(chargeToTemp));
        }else{
            JOptionPane.showMessageDialog(parent, "Charge To not match.\n Please set correct upload type in config file.");
            System.exit(1);
        }

        cmbChargeType.addItem("Split");
        cmbChargeType.addItem("Combine");
        String chargeTypeTemp=prop.getProperty("app.charge_type");
        if (checkingChargeTypeConfig(chargeTypeTemp)!=null){
            cmbChargeType.setSelectedItem(checkingChargeTypeConfig(chargeTypeTemp));
        }else{
            JOptionPane.showMessageDialog(parent, "Charge type not match.\n Please set correct upload type in config file.");
            System.exit(1);
        }

        tfDebitAccount.setText(prop.getProperty("app.debit_account_no"));
        tfDebitAccount.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                String value = tfDebitAccount.getText();
                int l = value.length();
                if (e.getKeyChar() >= '0' && e.getKeyChar() <= '9' || e.getKeyCode()==8) {
                    tfDebitAccount.setEditable(true);
                } else {
                    tfDebitAccount.setEditable(false);
                }

                if (l<=12 || e.getKeyCode()==8){
                    tfDebitAccount.setEditable(true);
                }else{
                    tfDebitAccount.setEditable(false);
                }
            }
        });

        tfAccountDescription.setText(prop.getProperty("app.account_description"));
        tfAccountDescription.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                String value = tfAccountDescription.getText();
                int l = value.length();
                if (l<=60 || e.getKeyCode()==8){
                    tfAccountDescription.setEditable(true);
                }else{
                    tfAccountDescription.setEditable(false);
                }
            }
        });

        fieldIntrustionDate.setDateFormatString("yyyy/MM/dd");
        JTextFieldDateEditor editor = (JTextFieldDateEditor) fieldIntrustionDate.getDateEditor();
        editor.setEditable(false);
        format = new SimpleDateFormat("yyyyMMdd");

        cmbInstructionAt.addItem("0700");
        cmbInstructionAt.addItem("1000");
        cmbInstructionAt.addItem("1300");
        cmbInstructionAt.addItem("1600");
        cmbInstructionAt.addItem("");
        cmbInstructionAt.setSelectedItem(prop.getProperty("app.instruction_at"));

        btConvert.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                btConvert.setEnabled(false);
                startTime= null;
                startTime = System.nanoTime();
                boolean status = true;
                debitAccount= tfDebitAccount.getText();
                descriptionAccount= tfAccountDescription.getText();
                instructionAt = (String) cmbInstructionAt.getSelectedItem();
                if(fieldIntrustionDate.getDate()!=null) {
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyMMdd");
                    DateTimeFormatter dtfTime= DateTimeFormatter.ofPattern("HHmm");
                    LocalDateTime now = LocalDateTime.now();
                    String timeNow= dtfTime.format(now);
                    if (format.format(fieldIntrustionDate.getDate()).compareTo(dtf.format(now))==0){
                        if (timeNow.compareTo(instructionAt)>0){
                            status= false;
                            JOptionPane.showMessageDialog(parent, "Instruction At (set in configuration file) has been passed");
                        }
                    }
                    if (format.format(fieldIntrustionDate.getDate()).compareTo(dtf.format(now))<0){
                        status= false;
                        JOptionPane.showMessageDialog(parent, "Instruction Date must be empty or equal or greater than today.");
                    }
                    instructionDate = format.format(fieldIntrustionDate.getDate());
                }else{
                    instructionAt="";
                }

                productCode= convertUploadType(cmbUploadType.getSelectedItem());

                chargeType= convertChargeType(cmbChargeType.getSelectedItem());

                chargeTo= convertChargeTo(cmbChargeTo.getSelectedItem());
                
                if (status==true) {
                    convert();
                }
                btConvert.setEnabled(true);
            }
        });
    }

    private static String checkingChargeTypeConfig(String property) {
        if (property.equals("S")){
            return "Split";
        }else if(property.equals("C")){
            return "Combine";
        }
        return null;
    }
    private static String checkingUploadTypeConfig(String property){
        if (property.equals("HAC")){
            return "Inhouse";
        }else if (property.equals("HYR")){
            return "Payroll";
        }
        return null;
    }
    private static String checkingChargeToConfig(String property){
        if (property.equals("REM")){
            return "REM";
        }else if(property.equals("BEN")){
            return "BEN";
        }
        return null;
    }

    private static String convertChargeTo(Object selectedItem){
        if (selectedItem.equals("REM")){
            return "REM";
        }
        return "BEN";

    }
    private static String convertChargeType(Object selectedItem){
        if (selectedItem.equals("Split")){
            return "S";
        }
        return "C";

    }
    private static String convertUploadType(Object selectedItem){
        if (selectedItem.equals("Inhouse")){
            return "HAC";
        }
        return "HYR";

    }

    private static void showGUI(){
        JFrame frame = new JFrame("App");//manggilframe
        frame.setContentPane(new mainForm().panel1);//manggil guiform
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//biar bisa di close
        frame.setResizable(false);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);//biar tampil
    }

    private static void convert(){
        File dir= new File(prop.getProperty("app.folder_in"));
        String[] fileList= dir.list();

        if (fileList.length==0){
            JOptionPane.showMessageDialog(parent, "File conversion error!! Please check the input file.");
        }
        int recordCount;
        for (String nameFile: fileList){
            recordCount=0;
            try {
                FileReader fileReader = new FileReader(prop.getProperty("app.folder_in")+nameFile);
                CSVReader csvReader = new CSVReader(fileReader);
                List<String[]> records = csvReader.readAll();

                String[] out= nameFile.split("\\.");
                File file = new File(prop.getProperty("app.folder_out")+out[0]+"_converted.csv");
                //menghasilkan output di folder OUT sesuai nama folder IN
                FileWriter outputfile = new FileWriter(file);
                CSVWriter writer = new CSVWriter(outputfile, ',', '\u0000', '\u0000', "\n");

                writer.writeNext(new String[]{"H", debitAccount, descriptionAccount, "S", "Y", "", instructionDate, instructionAt, ""});

                for (String[] record:records){
                    recordCount+=1;
                    int length= record.length-1;
                    String email="";
                    if(length == 6){
                        email= record[4]+";"+record[5]+";"+record[6];
                    }else if(length==5){
                        email= record[4]+";"+record[5];
                    }else if(length==4){
                        email= record[4];
                    }
                    writer.writeNext(new String[]{"D", productCode, "", "", "", "", "", "", "", record[0], "", "",
                            "", "", "",email, "", record[2], record[3], "", "", record[1], "", "", chargeTo, chargeType, "", "", "", "",
                            "", "", "", "", "", "", "", "", ""});
                }

                long endTime   = System.nanoTime();
                long totalTime = endTime - startTime;
                float sec= totalTime/100000000;
                int seconds= (int)sec;
                JOptionPane.showMessageDialog(parent, "Successfully converting "+recordCount+" record(s) \n in "+seconds+" seconds");

                writer.close();
            } catch (IOException | CsvException e) {
                e.printStackTrace();
                JOptionPane.showMessageDialog(parent, "File conversion error!! Please check the input file.");
            }

        }
    }

}
